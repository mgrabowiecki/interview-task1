package com.nvt.interview;

import com.nvt.interview.dto.StoreStock;
import com.nvt.interview.model.Car;
import com.nvt.interview.model.Store;
import com.nvt.interview.repository.CarRepository;
import com.nvt.interview.repository.StoreRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class StockService {

    private final CarRepository carRepository;
    private final StoreRepository storeRepository;


    public StockService(CarRepository carRepository, StoreRepository storeRepository) {
        this.carRepository = carRepository;
        this.storeRepository = storeRepository;
    }

    public List<StoreStock> getStoreStock() {
        return carRepository
                .findAll()
                .stream()
                .collect(groupingBy(Car::getStoreId))
                .entrySet()
                .stream()
                .map(k -> new StoreStock(
                        k.getKey(),
                        storeRepository.findOne(k.getKey()).get().getName(), k.getValue()))
                .collect(Collectors.toList());
    }
}
