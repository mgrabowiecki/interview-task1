package com.nvt.interview.dto;

import com.nvt.interview.model.Car;

import java.util.List;

public class StoreStock {

    private Long storeId;

    private String storeName;

    private List<Car> cars;

    public StoreStock(Long storeId, String storeName, List<Car> cars) {
        this.storeId = storeId;
        this.storeName = storeName;
        this.cars = cars;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "StoreStock{" +
                "storeId=" + storeId +
                ", storeName='" + storeName + '\'' +
                ", cars=" + cars +
                '}';
    }
}
