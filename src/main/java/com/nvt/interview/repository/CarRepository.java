package com.nvt.interview.repository;

import com.nvt.interview.model.Car;

import java.util.List;
import java.util.Optional;

public interface CarRepository {

    List<Car> findAll();

    Optional<Car> findOne(Long carId);

}
