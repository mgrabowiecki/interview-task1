package com.nvt.interview.repository;

import com.nvt.interview.model.Store;

import java.util.List;
import java.util.Optional;

public interface StoreRepository {

    List<Store> findAll();

    Optional<Store> findOne(Long storeId);

}
