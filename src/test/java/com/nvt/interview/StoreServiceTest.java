package com.nvt.interview;

import com.nvt.interview.model.Car;
import com.nvt.interview.model.Store;
import com.nvt.interview.repository.CarRepository;
import com.nvt.interview.repository.StoreRepository;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class StoreServiceTest {

    @Test
    public void shouldReturnStoreStock() {
        //given
        CarRepository carRepository = getMockCarRepository();
        StoreRepository storeRepository = getMockStoreRepository();
        StockService stockService=new StockService(carRepository,storeRepository);
        //when
        var storeStock=stockService.getStoreStock();
        //then
        assertEquals(4,storeStock.size());
        assertEquals("bla",storeStock.get(0).getStoreName());
    }

    private CarRepository getMockCarRepository() {
        CarRepository carRepository= Mockito.mock(CarRepository.class);
        when(carRepository.findAll()).thenReturn(List.of(
                new Car(1L,"a",1l),
                new Car(2L,"b",2l),
                new Car(3L,"c",1l),
                new Car(4L,"d",2l),
                new Car(5L,"e",3l),
                new Car(6L,"f",1l),
                new Car(7L,"g",3l),
                new Car(8L,"h",4l),
                new Car(9L,"i",1l)
        ));
        return carRepository;
    }

    private StoreRepository getMockStoreRepository() {
        StoreRepository storeRepository= Mockito.mock(StoreRepository.class);
        when(storeRepository.findOne(1l)).thenReturn(Optional.of(new Store(1l,"bla")));
        when(storeRepository.findOne(2l)).thenReturn(Optional.of(new Store(2l,"blu")));
        when(storeRepository.findOne(3l)).thenReturn(Optional.of(new Store(3l,"bli")));
        when(storeRepository.findOne(4l)).thenReturn(Optional.of(new Store(4l,"blx")));
        when(storeRepository.findOne(5l)).thenReturn(Optional.of(new Store(5l,"blz")));
        return storeRepository;
    }
}
