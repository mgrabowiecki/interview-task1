## CarRepository

Odpowiada za przechowywanie danych samochodów `Car`:
  
- id - unikalne id
- brand - nazwa marki
- storeId - id placówki w której się znajduje

## StoreRepository

Odpowiada za przehowywanie danych placówek `Store`:

- id - unikalne id placówki
- name - nazwa placówki


## Zadanie:

1) Na podstawie danych otrzymanych z `CarRepository` oraz `StoreRepository` przygotować raport (`StoreStock`) zawierajacy informację:

   - storeId - id placówki
   - storeName - nazwa placówki
   - cars - listę samochodów, które znajdują się w danej placówce

   Zaimplementować to należy w klasie  `StockService` -> `getStoreStock()`

2) Napisać testy